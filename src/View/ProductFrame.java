package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;

import Common.GlobalFunction;
import Model.CustomerDB;
import Model.CustomerManager;
import Model.ProductDB;
import Model.ProductManager;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ProductFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_id;
	private JTextField textField_name;
	private JTextField textField_price;
	private JTextField textField_dest;
	private ImagePanel imagePanel;
	ArrayList<ProductDB> list;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductFrame frame = new ProductFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 */
	public ProductFrame() throws IOException {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 679, 486);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 6, 328, 452);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRowCount() < 1){
					return;
				}
				int index = table.getSelectedRow();
				int id = Integer.parseInt(table.getValueAt(index, 0).toString());
				String name = table.getValueAt(index, 1).toString();
				String price = table.getValueAt(index, 2).toString();
				String dest = table.getValueAt(index, 3).toString();

				textField_id.setText("" + id);
				textField_name.setText("" + name);
				textField_price.setText("" + price);
				textField_dest.setText("" + dest);
				
				BufferedImage img = list.get(index).product_image;
				if (img != null) {
					imagePanel.setImage(img);
				}
			}
		});
		scrollPane.setViewportView(table);

		JLabel lblProductid = new JLabel("product id");
		lblProductid.setBounds(346, 11, 79, 16);
		contentPane.add(lblProductid);

		textField_id = new JTextField();
		textField_id.setEditable(false);
		textField_id.setColumns(10);
		textField_id.setBackground(Color.YELLOW);
		textField_id.setBounds(448, 6, 130, 26);
		contentPane.add(textField_id);

		JLabel lblProductName = new JLabel("product name");
		lblProductName.setBounds(346, 59, 98, 16);
		contentPane.add(lblProductName);

		textField_name = new JTextField();
		textField_name.setColumns(10);
		textField_name.setBounds(448, 54, 130, 26);
		contentPane.add(textField_name);

		JLabel lblPricePerUnit = new JLabel("price per unit");
		lblPricePerUnit.setBounds(346, 109, 98, 16);
		contentPane.add(lblPricePerUnit);

		textField_price = new JTextField();
		textField_price.setColumns(10);
		textField_price.setBounds(448, 104, 130, 26);
		contentPane.add(textField_price);

		JLabel lblProductDest = new JLabel("product dest.");
		lblProductDest.setBounds(346, 162, 98, 16);
		contentPane.add(lblProductDest);

		textField_dest = new JTextField();
		textField_dest.setColumns(10);
		textField_dest.setBounds(448, 157, 130, 26);
		contentPane.add(textField_dest);

		JButton button_save = new JButton("SAVE");
		button_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDB x = new ProductDB();
				if (GlobalFunction.isNumeric(textField_price.getText().trim())) {
					x.product_id = 0;
					x.product_name = textField_name.getText().trim();
					x.product_description = textField_dest.getText().trim();
					x.price_per_unit = Double.parseDouble(textField_price.getText().trim());
					x.product_image = (BufferedImage) imagePanel.getImage();
					try {
						ProductManager.saveProduct(x);
						load();
						textField_id.setText("");
						textField_name.setText("");
						textField_dest.setText("");
						textField_price.setText("");
						JOptionPane.showMessageDialog(ProductFrame.this, "finish!!");
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(ProductFrame.this, "Please Insert Only Number");
					textField_price.requestFocus();
					textField_price.selectAll();
				}

				

			}
		});
		button_save.setBounds(340, 412, 117, 29);
		contentPane.add(button_save);

		JButton button_edit = new JButton("EDIT");
		button_edit.setBounds(448, 412, 117, 29);
		contentPane.add(button_edit);

		JButton button_delete = new JButton("DELETE");
		button_delete.setBounds(556, 412, 117, 29);
		contentPane.add(button_delete);

		imagePanel = new ImagePanel();
		imagePanel.setBounds(346, 209, 213, 171);
		contentPane.add(imagePanel);

		JButton btnBrowse = new JButton("Browse");
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new OpenFileFilter("jpeg", "Photo in JPEG format"));
				chooser.addChoosableFileFilter(new OpenFileFilter("jpg", "Photo in JPG format"));
				chooser.addChoosableFileFilter(new OpenFileFilter("png", "PNG image"));
				chooser.addChoosableFileFilter(new OpenFileFilter("svg", "Scalable Vector Graphic"));
				chooser.setAcceptAllFileFilterUsed(false);
				int returnVal = chooser.showOpenDialog(ProductFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File f = chooser.getSelectedFile();
					try {
						BufferedImage img = ImageIO.read(f);
						imagePanel.setImage(img);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
		});
		btnBrowse.setBounds(562, 209, 117, 29);
		contentPane.add(btnBrowse);

		load();
	}

	public void load() throws IOException {
		// load table here
		list = ProductManager.getAllProduct();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("product_id");
		model.addColumn("product_name");
		model.addColumn("price_per_unit");
		model.addColumn("product_description");
		for (ProductDB c : list) {
			model.addRow(new Object[] { c.product_id, c.product_name, c.price_per_unit, c.product_description });
		}
		table.setModel(model);
	}

	class OpenFileFilter extends FileFilter {

		String description = "";
		String fileExt = "";

		public OpenFileFilter(String extension) {
			fileExt = extension;
		}

		public OpenFileFilter(String extension, String typeDescription) {
			fileExt = extension;
			this.description = typeDescription;
		}

		@Override
		public boolean accept(File f) {
			if (f.isDirectory())
				return true;
			return (f.getName().toLowerCase().endsWith(fileExt));
		}

		@Override
		public String getDescription() {
			return description;
		}
	}
}
