package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Model.CustomerDB;
import Model.CustomerManager;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CustomerFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField_id;
	private JTextField textField_name;
	private JTextField textField_surname;
	private JTextField textField_phone;
	ArrayList<CustomerDB> list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CustomerFrame frame = new CustomerFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CustomerFrame() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 573, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 6, 328, 379);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRowCount() < 1) {
					return;
				}
				int index = table.getSelectedRow();
				int id = Integer.parseInt(table.getValueAt(index, 0).toString());
				String name = table.getValueAt(index, 1).toString();
				String surname = table.getValueAt(index, 2).toString();
				String phone = table.getValueAt(index, 3).toString();

				textField_id.setText("" + id);
				textField_name.setText("" + name);
				textField_surname.setText("" + surname);
				textField_phone.setText("" + phone);
			}
		});
		scrollPane.setViewportView(table);

		JLabel lblId = new JLabel("id");
		lblId.setBounds(364, 10, 61, 16);
		contentPane.add(lblId);

		textField_id = new JTextField();
		textField_id.setEditable(false);
		textField_id.setBackground(Color.YELLOW);
		textField_id.setBounds(419, 6, 130, 26);
		contentPane.add(textField_id);
		textField_id.setColumns(10);

		JLabel lblName = new JLabel("name");
		lblName.setBounds(364, 58, 61, 16);
		contentPane.add(lblName);

		textField_name = new JTextField();
		textField_name.setColumns(10);
		textField_name.setBounds(419, 54, 130, 26);
		contentPane.add(textField_name);

		JLabel lblSurname = new JLabel("surname");
		lblSurname.setBounds(364, 108, 61, 16);
		contentPane.add(lblSurname);

		textField_surname = new JTextField();
		textField_surname.setColumns(10);
		textField_surname.setBounds(419, 104, 130, 26);
		contentPane.add(textField_surname);

		JLabel lblPhone = new JLabel("phone");
		lblPhone.setBounds(364, 162, 61, 16);
		contentPane.add(lblPhone);

		textField_phone = new JTextField();
		textField_phone.setColumns(10);
		textField_phone.setBounds(419, 158, 130, 26);
		contentPane.add(textField_phone);

		JButton btnSaveNew = new JButton("SAVE");
		btnSaveNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerDB x = new CustomerDB(0, textField_name.getText().trim(), textField_surname.getText().trim(),
						textField_phone.getText().trim());
				CustomerManager.saveCustomer(x);
				load();
				textField_id.setText("");
				textField_name.setText("");
				textField_surname.setText("");
				textField_phone.setText("");
				JOptionPane.showMessageDialog(CustomerFrame.this, "save success!!");
			}
		});
		btnSaveNew.setBounds(391, 204, 117, 29);
		contentPane.add(btnSaveNew);

		JButton btnEdit = new JButton("EDIT");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CustomerDB x = new CustomerDB(Integer.parseInt(textField_id.getText()), textField_name.getText().trim(),
						textField_surname.getText().trim(), textField_phone.getText().trim());
				CustomerManager.editCustomer(x);
				load();
				textField_id.setText("");
				textField_name.setText("");
				textField_surname.setText("");
				textField_phone.setText("");
				JOptionPane.showMessageDialog(CustomerFrame.this, "edit success!!");
			}
		});
		btnEdit.setBounds(391, 245, 117, 29);
		contentPane.add(btnEdit);

		JButton btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (JOptionPane.showConfirmDialog(CustomerFrame.this, "Do you want to delete?", "DELETE ?",
						JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
					CustomerDB x = new CustomerDB(Integer.parseInt(textField_id.getText()),
							textField_name.getText().trim(), textField_surname.getText().trim(),
							textField_phone.getText().trim());
					CustomerManager.deleteCustomer(x);
					load();
					textField_id.setText("");
					textField_name.setText("");
					textField_surname.setText("");
					textField_phone.setText("");
					JOptionPane.showMessageDialog(CustomerFrame.this, "delete success!!");
				}
			}
		});
		btnDelete.setBounds(391, 286, 117, 29);
		contentPane.add(btnDelete);

		load();
	}

	public void load() {
		// load table here
		list = CustomerManager.getAllCustomer();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("id");
		model.addColumn("name");
		model.addColumn("surname");
		model.addColumn("phone");
		for (CustomerDB c : list) {
			model.addRow(new Object[] { c.id, c.name, c.surname, c.phone });
		}
		table.setModel(model);
	}

}
