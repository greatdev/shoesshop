package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.CompanyInfoDB;
import Model.CompanyInfoManager;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CompanyInfoFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_comp_name;
	private JTextField textField_address;
	private JTextField textField_phone;
	private JTextField textField_email;
	CompanyInfoDB x = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompanyInfoFrame frame = new CompanyInfoFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CompanyInfoFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField_comp_name = new JTextField();
		textField_comp_name.setBounds(203, 41, 185, 26);
		contentPane.add(textField_comp_name);
		textField_comp_name.setColumns(10);
		
		textField_address = new JTextField();
		textField_address.setBounds(203, 79, 185, 26);
		contentPane.add(textField_address);
		textField_address.setColumns(10);
		
		textField_phone = new JTextField();
		textField_phone.setBounds(203, 117, 185, 26);
		contentPane.add(textField_phone);
		textField_phone.setColumns(10);
		
		textField_email = new JTextField();
		textField_email.setBounds(203, 155, 185, 26);
		contentPane.add(textField_email);
		textField_email.setColumns(10);
		
		JLabel lblCompanyName = new JLabel("Company Name");
		lblCompanyName.setBounds(68, 46, 99, 16);
		contentPane.add(lblCompanyName);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(68, 84, 61, 16);
		contentPane.add(lblAddress);
		
		JLabel lblPhone = new JLabel("Phone");
		lblPhone.setBounds(68, 122, 61, 16);
		contentPane.add(lblPhone);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(68, 160, 61, 16);
		contentPane.add(lblEmail);
		
		JButton btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				x.company_name = textField_comp_name.getText();
				x.address = textField_address.getText();
				x.phone = textField_phone.getText();
				x.email = textField_email.getText();
				CompanyInfoManager.editCompany(x);
			}
		});
		btnSave.setBounds(197, 193, 117, 29);
		contentPane.add(btnSave);
		
		loadData();
	}
	
	public void loadData(){
		x = CompanyInfoManager.getCompanyInfo();
		textField_comp_name.setText(x.company_name);
		textField_address.setText(x.address);
		textField_phone.setText(x.phone);
		textField_email.setText(x.email);
	}

}
