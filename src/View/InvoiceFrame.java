package View;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import Model.CompanyInfoDB;
import Model.CompanyInfoManager;
import Model.CustomerDB;
import Model.InvoiceDetail;
import Model.InvoiceManager;
import Model.ProductDB;
import View.SearchCustomerFrame.SelectCustomerI;
import View.SearchProductFrame.SelectProductI;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InvoiceFrame extends JFrame {

	private JPanel contentPane;
	public JPanel panel;
	private JLabel lblCompanyInfo;
	private JLabel lblCustomerInfo;
	private JLabel label_detail;
	public JTable table;
	private JLabel lblDate;
	public JScrollPane scrollPane;
	private JButton btnSelectCustomer;
	private JButton btnSelectProduct;
	private JButton btnSave;
	private JButton btnPrint;
	CustomerDB xCustomer;
	private JLabel label_1;

	ArrayList<InvoiceDetail> detailList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InvoiceFrame frame = new InvoiceFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InvoiceFrame() {
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				panel.setBounds(0, 60, getWidth() - 2, getHeight() - 100);
			}
		});
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();

		setBounds(0, 0, 1000, (int) height);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(0, 60, getWidth() - 2, getHeight() - 100);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel label = new JLabel("ใบเสร็จรับเงิน");
		label.setFont(new Font("Tahoma", Font.BOLD, 40));
		label.setBounds(430, 22, 284, 102);
		panel.add(label);

		lblCompanyInfo = new JLabel("company Info");
		lblCompanyInfo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCompanyInfo.setBounds(22, 132, 467, 16);
		panel.add(lblCompanyInfo);

		lblCustomerInfo = new JLabel("customer info");
		lblCustomerInfo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCustomerInfo.setBounds(141, 164, 327, 16);
		panel.add(lblCustomerInfo);

		label_detail = new JLabel("รายละเอียด");
		label_detail.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_detail.setBounds(22, 192, 104, 16);
		panel.add(label_detail);

		lblDate = new JLabel("date");
		lblDate.setBounds(529, 136, 294, 16);
		panel.add(lblDate);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 220, 952, 394);
		panel.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		btnSelectCustomer = new JButton("select customer");
		btnSelectCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchCustomerFrame f = new SearchCustomerFrame();
				f.setSelectCustomer(new SelectCustomerI() {

					@Override
					public void select(CustomerDB x) {
						// TODO Auto-generated method stub
						xCustomer = x;
						String s = x.name + " " + x.surname + "( " + x.phone + ") ( id = " + x.id + ")";
						lblCustomerInfo.setText(s);
					}
				});
				f.setVisible(true);
			}
		});
		btnSelectCustomer.setBounds(0, 19, 156, 29);
		contentPane.add(btnSelectCustomer);

		btnSelectProduct = new JButton("select product");
		btnSelectProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SearchProductFrame s;
				try {
					s = new SearchProductFrame();
					s.setSelectProduct(new SelectProductI() {

						@Override
						public void select(ProductDB x) {
							setDetail(x);
						}
					});
					s.setVisible(true);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnSelectProduct.setBounds(153, 19, 156, 29);
		contentPane.add(btnSelectProduct);

		btnSave = new JButton("save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InvoiceManager c = new InvoiceManager();
				if(xCustomer == null){
					JOptionPane.showMessageDialog(InvoiceFrame.this, "Didn't select customer");
				}else{
					InvoiceManager.saveInvoice(xCustomer, detailList);
				}
			}
		});
		btnSave.setBounds(354, 19, 117, 29);
		contentPane.add(btnSave);

		btnPrint = new JButton("print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PrinterJob job = PrinterJob.getPrinterJob();
				InvoicePrint a = new InvoicePrint(InvoiceFrame.this);
				job.setPrintable(a);
				boolean doprint = job.printDialog();
				if (doprint) {
					try {
						job.print();
					} catch (PrinterException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		btnPrint.setBounds(465, 19, 117, 29);
		contentPane.add(btnPrint);

		CompanyInfoDB x = CompanyInfoManager.getCompanyInfo();
		String CompanyInfo = x.company_name + " ที่อยู่ " + x.address + " โทร. " + x.phone + " อีเมลล์ " + x.email;
		System.out.println(CompanyInfo);
		lblCompanyInfo.setText(CompanyInfo);
		// current time
		lblDate.setText(new SimpleDateFormat().format(Calendar.getInstance().getTime()).toString());

		label_1 = new JLabel("ได้รับเงินจาก");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(22, 164, 93, 16);
		panel.add(label_1);

		detailList = new ArrayList<InvoiceDetail>();
	}

	public void setDetail(ProductDB xProduct) {
		InvoiceDetail x = new InvoiceDetail();
		x.no = detailList.size() + 1;
		x.price_per_unit = xProduct.price_per_unit;
		x.productName = xProduct.product_name;
		x.qty = 1;
		x.totalPrice = x.price_per_unit * x.qty;
		x.product = xProduct;

		detailList.add(x);

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("no");
		model.addColumn("image");
		model.addColumn("product name");
		model.addColumn("qty");
		model.addColumn("price_per_unit");
		model.addColumn("totalPrice");
		for (InvoiceDetail c : detailList) {
			model.addRow(new Object[] { c.no, c.product.product_image, c.productName, c.qty, c.price_per_unit,
					c.totalPrice });
		}

		table.setModel(model);

		table.getColumn("image").setCellRenderer(new InvoiceDetailTableRenderer());

		for (int i = 0; i < table.getRowCount(); i++) {
			table.setRowHeight(i, 120);
		}
	}

}

class InvoiceDetailTableRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int col) {
		ImageComponent i = new ImageComponent();
		i.image = (BufferedImage) value;
		return i;
	}
}

class ImageComponent extends JComponent {
	public BufferedImage image;

	public void paint(Graphics g) {
		if (image != null) {
			g.drawImage(image, 0, 0, 120, 120, 0, 0, image.getWidth(), image.getHeight(), this);
		}
	}
}

class InvoicePrint implements Printable {

	InvoiceFrame xframe;

	// print ตัวนีั้ออกมา
	public InvoicePrint(InvoiceFrame frame) {
		xframe = frame;
	}

	@Override
	public int print(Graphics g, PageFormat pf, int page) throws PrinterException {
		if (page > 0) {
			return NO_SUCH_PAGE;
		}

		Graphics2D g2d = (Graphics2D) g;
		g2d.translate(pf.getImageableX(), pf.getImageableY());

		for (int i = 0; i < xframe.panel.getComponentCount(); i++) {
			Component c = xframe.panel.getComponent(i);
			if (c instanceof JLabel) {
				JLabel l = (JLabel) c;
				g2d.setFont(l.getFont());
				g2d.drawString(l.getText(), (int) l.getLocation().getX(), (int) l.getLocation().getY() + l.getHeight());
			}
		}

		int x = 10;
		int y = (int) xframe.scrollPane.getLocation().getY() + 40;
		for (int i = 0; i < xframe.table.getColumnCount(); i++) {
			g2d.setFont(xframe.table.getFont());
			g2d.drawString(xframe.table.getColumnName(i), x, y);

			x += xframe.table.getColumn(xframe.table.getColumnName(i)).getWidth();
		}

		y += 40;
		for (int j = 0; j < xframe.table.getRowCount(); j++) {
			x = 10;
			for (int i = 0; i < xframe.table.getColumnCount(); i++) {
				//draw image when print
				if (xframe.table.getColumnName(i).equals("image")) {
					BufferedImage image = (BufferedImage) xframe.table.getValueAt(j, i);
					g.drawImage(image, x, y, x+120, y+120, 0, 0, image.getWidth(), image.getHeight(), null);
				} else {
					g2d.setFont(xframe.table.getFont());
					g2d.drawString(" " + xframe.table.getValueAt(j, i), x, y);
				}
				x += xframe.table.getColumn(xframe.table.getColumnName(i)).getWidth();

			}
			y += xframe.table.getRowHeight(j);
		}

		return PAGE_EXISTS;
	}

}
