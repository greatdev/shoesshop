package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Model.CustomerDB;
import Model.CustomerManager;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SearchCustomerFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_search;
	ArrayList<CustomerDB> list;
	private JTable table;
	private JButton btnSelect;
	SelectCustomerI xSelectCustomerI; 
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchCustomerFrame frame = new SearchCustomerFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SearchCustomerFrame() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 79, 400, 176);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		textField_search = new JTextField();
		textField_search.setBounds(28, 33, 130, 26);
		contentPane.add(textField_search);
		textField_search.setColumns(10);
		
		JButton btnSearch = new JButton("search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		btnSearch.setBounds(168, 33, 117, 29);
		contentPane.add(btnSearch);
		
		btnSelect = new JButton("select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRowCount() == 0){
					JOptionPane.showMessageDialog(SearchCustomerFrame.this, "Please Select 1 row");
					return ;
				}
				
				// งง เขาบอกเพื่อความระเบียบระหว่างคลาสแม่คลาสลูก
				if(xSelectCustomerI != null){
					if(list != null){
						xSelectCustomerI.select(list.get(table.getSelectedRow()));
						setVisible(false);
					}
				}
			}
		});
		btnSelect.setBounds(314, 33, 117, 29);
		contentPane.add(btnSelect);
		
		loadData();
	}
	
	public void loadData(){
		list = CustomerManager.getAllCustomer();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("id");
		model.addColumn("name");
		model.addColumn("surname");
		model.addColumn("phone");
		for (CustomerDB c : list) {
			model.addRow(new Object[] { c.id, c.name, c.surname, c.phone });
		}
		table.setModel(model);
	}
	
	public void search(){
		list = CustomerManager.searchCustomer(textField_search.getText());
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("id");
		model.addColumn("name");
		model.addColumn("surname");
		model.addColumn("phone");
		for (CustomerDB c : list) {
			model.addRow(new Object[] { c.id, c.name, c.surname, c.phone });
		}
		table.setModel(model);
	}
	
	public void setSelectCustomer(SelectCustomerI x){
		xSelectCustomerI = x;
	}
	
	//Messenger in Desgin Pattern
	interface SelectCustomerI{
		public void select(CustomerDB x);
	}

}
