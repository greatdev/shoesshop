package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Model.CustomerDB;
import Model.CustomerManager;
import Model.ProductDB;
import Model.ProductManager;
import View.SearchCustomerFrame.SelectCustomerI;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SearchProductFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_search;
	private JTable table;
	SelectProductI xSelectProductI;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SearchProductFrame frame = new SearchProductFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public SearchProductFrame() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 74, 400, 176);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		textField_search = new JTextField();
		textField_search.setColumns(10);
		textField_search.setBounds(25, 28, 130, 26);
		contentPane.add(textField_search);
		
		JButton button_search = new JButton("search");
		button_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					search();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_search.setBounds(165, 28, 117, 29);
		contentPane.add(button_search);
		
		JButton button_select = new JButton("select");
		button_select.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRowCount() == 0){
					JOptionPane.showMessageDialog(SearchProductFrame.this, "Please Select 1 row");
					return ;
				}
				
				// งง เขาบอกเพื่อความระเบียบระหว่างคลาสแม่คลาสลูก
				if(xSelectProductI != null){
					if(list != null){
						xSelectProductI.select(list.get(table.getSelectedRow()));
						setVisible(false);
					}
				}
			}
		});
		button_select.setBounds(311, 28, 117, 29);
		contentPane.add(button_select);
		
		loadData();
	}
	
	public void loadData() throws IOException{
		list = ProductManager.getAllProduct();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("product_id");
		model.addColumn("product_name");
		model.addColumn("price_per_unit");
		model.addColumn("product_description");
		for (ProductDB c : list) {
			model.addRow(new Object[] { c.product_id, c.product_name, c.price_per_unit, c.product_description });
		}
		table.setModel(model);
	}
	
	public void setSelectProduct(SelectProductI x){
		xSelectProductI = x;
	}
	
	ArrayList<ProductDB> list;
	public void search() throws IOException{
		list = ProductManager.searchProduct(textField_search.getText());
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("product_id");
		model.addColumn("product_name");
		model.addColumn("price_per_unit");
		model.addColumn("product_description");
		for (ProductDB c : list) {
			model.addRow(new Object[] { c.product_id, c.product_name, c.price_per_unit, c.product_description });
		}
		table.setModel(model);
	}
	
	//Messenger in Desgin Pattern
		interface SelectProductI{
			public void select(ProductDB x);
		}

}
