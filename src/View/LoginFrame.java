package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.UserManager;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_username;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		setTitle("Shoes Shop Login");
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		textField_username = new JTextField();
		textField_username.setBounds(206, 63, 130, 26);
		contentPane.add(textField_username);
		textField_username.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					check();
				}
			}
		});
		passwordField.setBounds(206, 101, 130, 26);
		contentPane.add(passwordField);
		
		JLabel lblNewLabel = new JLabel("username");
		lblNewLabel.setBounds(112, 68, 61, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(112, 106, 61, 16);
		contentPane.add(lblPassword);
		
		JButton btnLogin = new JButton("login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				check();
			}
		});
		btnLogin.setBounds(104, 157, 117, 29);
		contentPane.add(btnLogin);
		
		JButton btnExit = new JButton("exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// exit program
				System.exit(0);
			}
		});
		btnExit.setBounds(230, 157, 117, 29);
		contentPane.add(btnExit);
	}
	
	public void check(){
		if(UserManager.checkLogin(textField_username.getText().trim(),new String(passwordField.getPassword()))){
			// Login True going to main frame
			MainFrame f = new MainFrame();
			f.setVisible(true);
			
			LoginFrame.this.setVisible(false);
		}else{
			JOptionPane.showMessageDialog(LoginFrame.this, "username or password incorrect");
		}
	}

}
