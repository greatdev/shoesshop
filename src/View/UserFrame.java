package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Model.CustomerDB;
import Model.CustomerManager;
import Model.ProductDB;
import Model.ProductManager;
import Model.UserDB;
import Model.UserManager;

import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_id;
	private JTextField textField_username;
	private JTextField textField_password;

	ArrayList<UserDB> list;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserFrame frame = new UserFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public UserFrame() throws IOException {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 580, 434);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		JComboBox<String> comboBox = new JComboBox<String>();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 6, 328, 400);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (table.getSelectedRowCount() < 1){
					return;
				}
				int index = table.getSelectedRow();
				int id = Integer.parseInt(table.getValueAt(index, 0).toString());
				String username = table.getValueAt(index, 1).toString();
				String password = table.getValueAt(index, 2).toString();
				String usertype = table.getValueAt(index, 3).toString();

				textField_id.setText("" + id);
				textField_username.setText("" + username);
				textField_password.setText("" + password);
				comboBox.setSelectedItem(usertype);
			}
		});
		scrollPane.setViewportView(table);
		
		JLabel label = new JLabel("id");
		label.setBounds(346, 17, 61, 16);
		contentPane.add(label);
		
		textField_id = new JTextField();
		textField_id.setEditable(false);
		textField_id.setColumns(10);
		textField_id.setBackground(Color.YELLOW);
		textField_id.setBounds(418, 13, 130, 26);
		contentPane.add(textField_id);
		
		JLabel lblUsername = new JLabel("username");
		lblUsername.setBounds(346, 65, 61, 16);
		contentPane.add(lblUsername);
		
		textField_username = new JTextField();
		textField_username.setColumns(10);
		textField_username.setBounds(418, 61, 130, 26);
		contentPane.add(textField_username);
		
		JLabel lblPassword = new JLabel("password");
		lblPassword.setBounds(346, 115, 61, 16);
		contentPane.add(lblPassword);
		
		textField_password = new JTextField();
		textField_password.setColumns(10);
		textField_password.setBounds(418, 111, 130, 26);
		contentPane.add(textField_password);
		
		JLabel lblUsertype = new JLabel("usertype");
		lblUsertype.setBounds(346, 169, 61, 16);
		contentPane.add(lblUsertype);
		
		JButton button = new JButton("SAVE");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserDB x = new UserDB(0, textField_username.getText().trim(), textField_password.getText().trim(),
						comboBox.getSelectedItem().toString());
				UserManager.saveUser(x);
				try {
					load();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				textField_id.setText("");
				textField_username.setText("");
				textField_password.setText("");
				JOptionPane.showMessageDialog(UserFrame.this, "save success!!");
			}
		});
		button.setBounds(391, 204, 117, 29);
		contentPane.add(button);
		
		JButton button_1 = new JButton("EDIT");
		button_1.setBounds(391, 245, 117, 29);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("DELETE");
		button_2.setBounds(391, 286, 117, 29);
		contentPane.add(button_2);
		
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"ADMIN", "USER"}));
		comboBox.setBounds(418, 165, 130, 27);
		contentPane.add(comboBox);
		load();
	}
	
	public void load() throws IOException {
		// load table here
		list = UserManager.getAllUser();
		DefaultTableModel model = (DefaultTableModel) table.getModel();

		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("user_id");
		model.addColumn("username");
		model.addColumn("password");
		model.addColumn("usertype");
		for (UserDB c : list) {
			model.addRow(new Object[] { c.Id, c.username, c.password, c.userType });
		}
		
		table.setModel(model);
	}

}
