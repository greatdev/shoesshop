package Model;

public class UserDB {
	
	public int Id;
	public String username;
	public String password;
	public String userType;
	
	public UserDB() {}
	public UserDB(int xId, String xusername, String xpassword, String xusertype) {
		this.username = xusername;
		this.Id = xId;
		this.password = xpassword;
		this.userType = xusertype;
	}

}
