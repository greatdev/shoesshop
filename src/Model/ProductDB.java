package Model;

import java.awt.image.BufferedImage;

public class ProductDB {

	public int product_id;
	public String product_name;
	public double price_per_unit;
	public String product_description;
	public BufferedImage product_image;
	
	public ProductDB(){}
	
	public ProductDB(int xid, String xname,double xprice, String xdesc, BufferedImage ximage){
		this.product_id = xid;
		this.product_name = xname;
		this.price_per_unit = xprice;
		this.product_description = xdesc;
		this.product_image = ximage;
	}

}
