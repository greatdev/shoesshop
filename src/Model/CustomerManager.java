package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Common.GlobalData;

public class CustomerManager {

	public static ArrayList<CustomerDB> getAllCustomer() {

		ArrayList<CustomerDB> list = new ArrayList<CustomerDB>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "SELECT * FROM customer2";
			rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String surname = rs.getString("surname");
				String phone = rs.getString("phone");

				CustomerDB x = new CustomerDB(id, name, surname, phone);
				list.add(x);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return list;
	}

	public static void saveCustomer(CustomerDB x) {
		Connection con = null;
		Statement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "INSERT INTO customer2 VALUE(0, '" + x.name + "', '" + x.surname + "', '" + x.phone + "')";
			st.executeUpdate(query);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void editCustomer(CustomerDB x) {
		Connection con = null;
		Statement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "UPDATE customer2 SET name='" + x.name + "', surname='" + x.surname + "', phone='" + x.phone
					+ "' WHERE id='" + x.id + "'";
			st.executeUpdate(query);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void deleteCustomer(CustomerDB x) {
		Connection con = null;
		Statement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "DELETE FROM customer2 WHERE id='" + x.id + "'";
			st.executeUpdate(query);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static ArrayList<CustomerDB> searchCustomer(String s) {

		ArrayList<CustomerDB> list = new ArrayList<CustomerDB>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "SELECT * FROM customer2 WHERE name LIKE '"+s+"' OR surname LIKE '"+s+"'";
			rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String surname = rs.getString("surname");
				String phone = rs.getString("phone");

				CustomerDB x = new CustomerDB(id, name, surname, phone);
				list.add(x);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return list;
	}

	// unit test
	public static void main(String[] args) {
		ArrayList<CustomerDB> ll = CustomerManager.getAllCustomer();
		System.out.println(ll.size());
	}

}
