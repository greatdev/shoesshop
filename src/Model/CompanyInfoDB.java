package Model;

public class CompanyInfoDB {

	public int id;
	public String company_name;
	public String address;
	public String phone;
	public String email;

	public CompanyInfoDB(int id, String name, String addr, String phone, String email) {
		this.id = id;
		this.company_name = name;
		this.address = addr;
		this.phone = phone;
		this.email = email;
	}

}
