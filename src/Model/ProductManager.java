package Model;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.mysql.jdbc.PreparedStatement;

import Common.GlobalData;

public class ProductManager {

	public static ArrayList<ProductDB> getAllProduct() throws IOException {
		ArrayList<ProductDB> list = new ArrayList<ProductDB>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "SELECT * FROM products";
			rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("product_id");
				String name = rs.getString("product_name");
				double price = rs.getDouble("price_per_unit");
				String description = rs.getString("product_description");
				BufferedImage buffer_img = null;
				byte[] img_byte = rs.getBytes("product_image");
				if (img_byte == null || img_byte.length == 0) {
					// do nothing
				} else {
					ByteArrayInputStream bais = new ByteArrayInputStream(img_byte);
					buffer_img = ImageIO.read(bais);
					bais.close();
				}
				ProductDB x = new ProductDB(id, name, price, description, buffer_img);
				list.add(x);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return list;
	}

	public static void saveProduct(ProductDB x) throws IOException {

		Connection con = null;
		PreparedStatement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			String query = "INSERT INTO products VALUE(0, ?, ?, ?, ?)";
			st = (PreparedStatement) con.prepareStatement(query);
			st.setString(1, x.product_name);
			st.setDouble(2, x.price_per_unit);
			st.setString(3, x.product_description);
			if (x.product_image != null) {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				// write pic to outPutStream
				ImageIO.write(x.product_image, "png", outStream);
				byte[] buffer = outStream.toByteArray();
				st.setBytes(4, buffer);
				outStream.close();
			} else {
				byte[] buffer = new byte[0];
				st.setBytes(4, buffer);
			}
			st.executeUpdate();
			st.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static ArrayList<ProductDB> searchProduct(String s) throws IOException {

		ArrayList<ProductDB> list = new ArrayList<ProductDB>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "SELECT * FROM products WHERE product_name LIKE '"+s+"' OR product_description LIKE '"+s+"'";
			rs = st.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("product_id");
				String name = rs.getString("product_name");
				double price = rs.getDouble("price_per_unit");
				String description = rs.getString("product_description");
				BufferedImage buffer_img = null;
				byte[] img_byte = rs.getBytes("product_image");
				if (img_byte == null || img_byte.length == 0) {
					// do nothing
				} else {
					ByteArrayInputStream bais = new ByteArrayInputStream(img_byte);
					buffer_img = ImageIO.read(bais);
					bais.close();
				}
				ProductDB x = new ProductDB(id, name, price, description, buffer_img);
				list.add(x);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return list;
	}
}
