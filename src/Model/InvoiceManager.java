package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Common.GlobalData;

public class InvoiceManager {

	public static void saveInvoice(CustomerDB cust, ArrayList<InvoiceDetail> details) {
		Connection con = null;
		Statement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			
			//should be transaction control - search > java mysql transaction
			// เวลามี สองเครื่องใส่พร้อมกัน ค่า MAX จะเพี้ยน
			////////////
			st = con.createStatement();
			String query = "INSERT INTO invoice VALUE(0, NOW(), '" + cust.id + "', 'NORMAL')";
			st.executeUpdate(query);
			////////////
			
			query = "SELECT MAX(invoice_id) FROM invoice";
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			int id_max = 0;
			while (rs.next()) {
				id_max = rs.getInt(1);
			}

			/////////////

			for (int i = 0; i < details.size(); i++) {
				st = con.createStatement();
				query = "INSERT INTO invoice_detail VALUE(0, '" + id_max +"', '" + details.get(i).product.product_id
						+ "', '"+details.get(i).qty+"')";
				st.executeUpdate(query);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

}
