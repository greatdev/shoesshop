package Model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Common.GlobalData;

public class CompanyInfoManager {

public static CompanyInfoDB getCompanyInfo(){
	Connection con = null;
	Statement st = null;
	ResultSet rs = null;
	
	String myURL = "jdbc:mysql://"+GlobalData.DATABASE_LOCATION+":"+GlobalData.DATABASE_PORT+"/"+GlobalData.DATABASE_NAME;
	String username = GlobalData.DATABASE_USERNAME;
	String password = GlobalData.DATABASE_PASSWORD;
	
	try {
		con = DriverManager.getConnection(myURL, username, password);
		st = con.createStatement();
		String query = "SELECT * FROM company_info";
		rs = st.executeQuery(query);

		while (rs.next()) {
			
			int id = rs.getInt("id");
			String name = rs.getString("company_name");
			String address = rs.getString("address");
			String phone = rs.getString("phone");
			String email = rs.getString("email");
			
			CompanyInfoDB x = new CompanyInfoDB(id,name,address,phone,email);
			return x;
			
		}

	} catch (SQLException ex) {
		ex.printStackTrace();
	} finally {
		try {
			if (rs != null) {
				rs.close();
			}

			if (st != null) {
				st.close();
			}

			if (con != null) {
				con.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	return null;
	}
	
	public static void editCompany(CompanyInfoDB x){
		Connection con = null;
		Statement st = null;
		
		String myURL = "jdbc:mysql://"+GlobalData.DATABASE_LOCATION+":"+GlobalData.DATABASE_PORT+"/"+GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;
		
		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "UPDATE company_info SET company_name='"+x.company_name+"', address='"+x.address+"', phone='"+x.phone+"', email='"+x.email+"' WHERE id='"+x.id+"'";
			st.executeUpdate(query);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				
				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
}
