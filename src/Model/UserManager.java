package Model;

import java.sql.*;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import Common.GlobalData;

public class UserManager {

	public static ArrayList<UserDB> getAllUser() {

		ArrayList<UserDB> list = new ArrayList<UserDB>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "SELECT * FROM users";
			rs = st.executeQuery(query);

			while (rs.next()) {
				int xid = rs.getInt("id");
				String xusername = rs.getString("username");
				String xpassword = rs.getString("password");
				String xusertype = rs.getString("usertype");

				UserDB x = new UserDB(xid, xusername, xpassword, xusertype);
				list.add(x);

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}

		return list;
	}

	public static void saveUser(UserDB x) {
		Connection con = null;
		Statement st = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String username = GlobalData.DATABASE_USERNAME;
		String password = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, username, password);
			st = con.createStatement();
			String query = "INSERT INTO users VALUE(0, '" + x.username + "', '" + x.password + "', '" + x.userType
					+ "')";
			st.executeUpdate(query);

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {

				if (st != null) {
					st.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public static boolean checkLogin(String username, String password) {
		Connection con = null;
		ResultSet rs = null;

		String myURL = "jdbc:mysql://" + GlobalData.DATABASE_LOCATION + ":" + GlobalData.DATABASE_PORT + "/"
				+ GlobalData.DATABASE_NAME;
		String usernameDB = GlobalData.DATABASE_USERNAME;
		String passwordDB = GlobalData.DATABASE_PASSWORD;

		try {
			con = DriverManager.getConnection(myURL, usernameDB, passwordDB);
			
			String query = "SELECT * FROM users WHERE username = ? AND password = ? ";
			java.sql.PreparedStatement  st = con.prepareStatement(query);
			st.setString(1, username);
			st.setString(2, password);
			
			rs = st.executeQuery();

			while (rs.next()) {
				GlobalData.CurrentUser_UserID = rs.getInt(1);
				GlobalData.CurrentUser_Username = rs.getString(2);
				GlobalData.CurrentUser_UserType = rs.getString(4);
				return true;

			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
		return false;
	}

}
