package Model;

public class CustomerDB {
	public int id;
	public String name;
	public String surname;
	public String phone;
	
	public CustomerDB(){}
	
	public CustomerDB(int xid, String xname, String xsurname, String xphone){
		this.id = xid;
		this.name = xname;
		this.surname = xsurname;
		this.phone = xphone;
	}
}
