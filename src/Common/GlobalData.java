package Common;

public class GlobalData {
	public static final String DATABASE_LOCATION = "127.0.0.1";
	public static final String DATABASE_PORT = "3306";
	public static final String DATABASE_USERNAME = "root";
	public static final String DATABASE_PASSWORD = "";
	public static final String DATABASE_NAME = "shoe_shop";
	
	public static String CurrentUser_Username;
	public static int CurrentUser_UserID;
	public static String CurrentUser_UserType;
}
