mysql


```
#!mysql

DROP TABLE IF EXISTS `company_info`;

CREATE TABLE `company_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `company_info` WRITE;
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;

INSERT INTO `company_info` (`id`, `company_name`, `address`, `phone`, `email`)
VALUES
	(1,'บริษัท รองเท้าแก้ว','120 ม.5 ถนน ท่าพระบาง อำเภอ แม่สาย จังหวัดแพร่ 235','085555555','ntprintf@gmail.com');

/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customer2
# ------------------------------------------------------------

CREATE TABLE `customer2` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 DEFAULT '',
  `surname` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(11) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `customer2` WRITE;
/*!40000 ALTER TABLE `customer2` DISABLE KEYS */;

INSERT INTO `customer2` (`id`, `name`, `surname`, `phone`)
VALUES
	(1,'JAME','HENRY','083'),
	(2,'JOGN','GAY','890'),
	(3,'APPLE','GAOEY','888'),
	(4,'JEFF','GAOEY','093'),
	(5,'PEERA','VEERA','560'),
	(6,'AA','BBBCCC','555');

/*!40000 ALTER TABLE `customer2` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table invoice
# ------------------------------------------------------------

CREATE TABLE `invoice` (
  `invoice_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) DEFAULT NULL,
  `invoice_status` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table invoice_detail
# ------------------------------------------------------------

CREATE TABLE `invoice_detail` (
  `invoice_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoice_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table products
# ------------------------------------------------------------

CREATE TABLE `products` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `price_per_unit` double DEFAULT NULL,
  `product_description` text CHARACTER SET utf8,
  `product_image` longblob,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table users
# ------------------------------------------------------------

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `usertype` enum('ADMIN','USER') COLLATE utf8_bin NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `usertype`)
VALUES
	(1,'admin','admin',X'41444D494E'),
	(2,'user1','123',X'55534552'),
	(3,'user2','123',X'55534552');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

```